//Setup imports

const express = require("express");
const mongoose = require("mongoose");
const dotenv = require("dotenv");

const taskRoutes = require("./routes/taskRoutes.js");



//Express Setup
const app = express();
const port = 3001;


//Inititalize dotenv
dotenv.config();

//Middlewares
app.use(express.json());
app.use(express.urlencoded({ extended: true }))

//Mongoose Setup
mongoose.connect(`mongodb+srv://jeff0818:${process.env.MONGODB_PW}@batch224.1dappv1.mongodb.net/s36?retryWrites=true&w=majority`,
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }
);


let db = mongoose.connection
db.on('error', () => console.error('Connection Error.'));
db.on('open', () => console.log('Connected to MongoDB!'));

//Main URI
app.use("/tasks", taskRoutes);

app.listen(port, () => console.log(`Server is running at port: ${port}`))